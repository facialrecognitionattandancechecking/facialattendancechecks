import firebase from 'firebase';
import { firebaseConfig } from './constant/FirebaseConstant';

// initialize the firebase of the provided constant key
firebase.initializeApp(firebaseConfig);

export default firebase;

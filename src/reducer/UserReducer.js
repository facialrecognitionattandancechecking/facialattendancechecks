import {
    USER_REQUEST_REGISTER,
    LOAD_DESCRIPTORS,
} from '../action/ActionConstant';

const INITIALIZED_STATE = {
    isUserLoggedIn: false,
    userId: '',
    descriptors: null,
};

const userDescriptorRegisterRequest = (state, action) => {
    return {
        ...state,
        isUserLoggedIn: true,
        userId: action.userId,
    };
};

const loadAllDescriptors = (state, action) => {
    return {
        ...state,
        descriptors: action.descriptors,
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case USER_REQUEST_REGISTER:
            return userDescriptorRegisterRequest(state, action);
        case LOAD_DESCRIPTORS:
            return loadAllDescriptors(state, action);
        default:
            return state;
    }
};

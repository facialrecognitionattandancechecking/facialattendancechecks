import UserReducer from "./UserReducer";

const RootReducer = {
  user: UserReducer
};

export default RootReducer;
import React, { Component } from 'react';
import _ from 'lodash';
import { getFullFaceDescription, loadModels } from '../api/face';

// Initial State
const INIT_STATE = {
    imageURL: '',
    fullDesc: null,
    name: '',
};

class UserRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...INIT_STATE,
        };
    }
    componentDidMount() {
        //this.props.userDescriptorRegisterRequest({ Dara });
        this.props.loadAllDescriptors();
    }
    componentWillMount = async () => {
        await loadModels();
    };

    handleFileChange = async event => {
        await this.setState({
            imageURL: URL.createObjectURL(event.target.files[0]),
            loading: true,
        });
    };

    handleNameChange = event => {
        this.setState({ name: event.target.value });
    };

    onSubmit = async event => {
        event.preventDefault();
        const { imageURL, fullDesc, name } = this.state;
        const { userDescriptorRegisterRequest } = this.props;
        //console.log('Successfully added name ', name);
        const userDescriptors = {};
        await getFullFaceDescription(imageURL).then(fullDesc => {
            if (!!fullDesc) {
                _.merge(userDescriptors, {
                    [name]: {
                        name: name,
                        descriptors: fullDesc.map(fd => fd.descriptor),
                    },
                });
                userDescriptorRegisterRequest(userDescriptors);
                console.log('Hello AI', userDescriptors);
            }
        });
        this.resetState();
    };

    resetState = () => {
        this.setState({ ...INIT_STATE });
    };

    render() {
        const { descriptors } = this.props;
        const { imageURL, fullDesc, name } = this.state;
        return (
            <div>
                <p>Descriptor Register</p>
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input
                            placeholder="Name"
                            name="name"
                            onChange={this.handleNameChange}
                        />
                        <input
                            id="myFileUpload"
                            type="file"
                            onChange={this.handleFileChange}
                            accept=".jpg, .jpeg, .png"
                        />
                        <input type="submit" value="Sumit" />
                    </form>
                </div>
                <div>
                    <p>name: {name}</p>
                    <div>
                        <img src={imageURL} alt="imageURL" />
                    </div>
                </div>
            </div>
        );
    }
}

export default UserRegister;

import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { history } from './History';
import HomeContainer from '../container/HomeContainer';
import ImageInputContainer from '../container/ImageInputContainer';
import VideoInputContainer from '../container/VideoInputContainer';
import UserRegisterContainer from '../container/UserRegisterContainer';

class Root extends Component {
    render() {
        return (
            <div className="App">
                <Router history={history}>
                    <div className="route">
                        <Route exact path="/" component={HomeContainer} />
                        <Route
                            exact
                            path="/photo"
                            component={ImageInputContainer}
                        />
                        <Route
                            exact
                            path="/camera"
                            component={VideoInputContainer}
                        />
                        <Route
                            exact
                            path="/register"
                            component={UserRegisterContainer}
                        />
                    </div>
                </Router>
            </div>
        );
    }
}

export default Root;

import { connect } from 'react-redux';
import ImageInput from '../views/ImageInput';
const mapStateToProps = (state: Object) => {
    const { user } = state;
    const { descriptors } = user;
    return {
        ...state,
        descriptors,
    };
};
export default connect(mapStateToProps, null)(ImageInput);

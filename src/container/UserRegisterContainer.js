import { connect } from 'react-redux';
import UserRegister from '../views/UserRegister';
import {
    userDescriptorRegisterRequest,
    loadAllDescriptors,
} from '../action/ActionList';

const mapStateToProps = (state: Object) => {
    const { user } = state;
    const { descriptors } = user;
    return {
        ...state,
        descriptors,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        userDescriptorRegisterRequest: (newUser: Object) => {
            dispatch(userDescriptorRegisterRequest(newUser));
        },
        loadAllDescriptors: () => {
            dispatch(loadAllDescriptors());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserRegister);

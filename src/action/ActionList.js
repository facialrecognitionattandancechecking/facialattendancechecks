import firebase from '../util/Firebase';
import { USER_REQUEST_REGISTER, LOAD_DESCRIPTORS } from './ActionConstant';

//const dbRef = firebase.firestore();

export const userDescriptorRegisterRequest = (newUser: Object) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`user_descriptors`)
                .push(newUser)
                .then(docRef => {
                    console.log('Added Successfully ', newUser);
                    dispatch({
                        type: USER_REQUEST_REGISTER,
                        descriptor: newUser,
                    });
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const loadAllDescriptors = () => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firebase
                .database()
                .ref(`user_descriptors`)
                .once('value', async snapshot => {
                    const descriptors = snapshot.val();
                    dispatch({
                        type: LOAD_DESCRIPTORS,
                        descriptors: descriptors,
                    });
                    console.log('Successfully Loaded ', descriptors);
                })
                .catch(error => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};
